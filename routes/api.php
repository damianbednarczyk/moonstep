<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\SubscriberController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::prefix('tag')->name('tag.')->group(function(){
    Route::post('', [TagController::class, 'store'])->name('create');
    Route::get('', [TagController::class, 'index'])->name('index');
});

Route::prefix('subscriber')->name('subscriber.')->group(function(){
    Route::post('', [SubscriberController::class, 'store'])->name('create');
});



