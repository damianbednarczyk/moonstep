<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiHttpException;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSubscriberRequest;
use App\Http\Services\Subscriber\SubscriberServices;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class SubscriberController extends Controller
{

    public function __construct(protected SubscriberServices $subscriberServices){

    }


    public function store(StoreSubscriberRequest $storeSubscriberRequest)
    {

        try {
            $data = $storeSubscriberRequest->validated();
            $this->subscriberServices->createSubscriber($data);

        }catch (Throwable $e) {
            throw new ApiHttpException($e->getMessage(), ApiHttpException::CODE_DEFAULT, $e);
        }

        return response()->json('ok', Response::HTTP_OK);
    }


}
