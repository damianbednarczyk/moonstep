<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiHttpException;
use App\Http\Controllers\Controller;
use App\Http\Repository\Eloquent\TagRepository;
use App\Http\Requests\StoreTagRequest;
use App\Http\Resources\TagResource;
use App\Http\Services\Tag\TagServices;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class TagController extends Controller
{


    /**
     * TagController constructor.
     * @param TagServices $tagServices
     * @param TagRepository $tagRepository
     */
    public function __construct(protected TagServices $tagServices, protected TagRepository $tagRepository)
    {}


    /**
     * @param StoreTagRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTagRequest $request):JsonResource
    {
        try {
            $data = $request->validated();
            $this->tagServices->create($data);
        } catch (Throwable $e) {
            throw new ApiHttpException($e->getMessage(), ApiHttpException::CODE_DEFAULT, $e);
        }

        return response()->json('ok', Response::HTTP_OK);
    }


    /**
     * @return JsonResource
     */
    public function index():JsonResource
    {
        $tags = $this->tagRepository->getAllTags();
        return TagResource::collection($tags);
    }





}
