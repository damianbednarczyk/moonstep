<?php


namespace App\Http\Services\Subscriber;


use App\Http\Repository\Eloquent\SubscriberRepository;

class SubscriberServices
{

    public function __construct(protected SubscriberRepository $subscriberRepository){}


    /**
     * @param array $data
     */
    public function createSubscriber(array $data)
    {
        $this->subscriberRepository->createSubscriber($data);
    }


}
