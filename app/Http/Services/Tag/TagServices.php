<?php


namespace App\Http\Services\Tag;


use App\Http\Repository\Eloquent\TagRepository;

class TagServices
{

    public function __construct(protected TagRepository $tagRepository){

    }

    public function create(array $data)
    {
        $this->tagRepository->createTag($data);
    }
}
