<?php


namespace App\Http\Repository\Eloquent;


use App\Http\Repository\SubscriberInterface;
use App\Models\Subscriber;
use Illuminate\Support\Arr;

class SubscriberRepository implements SubscriberInterface
{


    /**
     * @param array $data
     * @return Subscriber
     */
    public function createSubscriber(array $data):Subscriber
    {
       $subscriber =  Subscriber::create([
            Subscriber::ATTRIBUTE_EMAIL => Arr::get($data, 'email'),
            Subscriber::ATTRIBUTE_FIRST_NAME => Arr::get($data, 'first_name'),
            Subscriber::ATTRIBUTE_LAST_NAME => Arr::get($data, 'last_name')
        ]);

       $tagId = Arr::get($data, 'tag_ids');
       if($tagId){
           $subscriber->tags()->sync($tagId);
       }
        return $subscriber->load('tags');
    }
}
