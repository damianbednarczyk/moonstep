<?php


namespace App\Http\Repository\Eloquent;


use App\Http\Repository\TagInterface;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class TagRepository implements TagInterface
{

    /**
     * @return Collection
     */
    public function getAllTags(): Collection
    {
        return Tag::all();
    }


    /**
     * @param array $data
     */
    public function createTag(array $data):void
    {
        Tag::create([
            Tag::ATTRIBUTE_NAME => Arr::get($data, Tag::ATTRIBUTE_NAME)
        ]);
    }

}
