<?php


namespace App\Http\Repository;


use Illuminate\Database\Eloquent\Collection;

interface TagInterface
{


    /**
     * @return Collection
     */
    public function getAllTags():Collection;


    /**
     * @param array $data
     */
    public function createTag(array $data):void;

}
