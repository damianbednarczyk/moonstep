<?php

namespace App\Http\Repository;

use App\Models\Subscriber;

interface SubscriberInterface
{


    /**
     * @param array $data
     * @return Subscriber
     */
    public function createSubscriber(array $data):Subscriber;
}
