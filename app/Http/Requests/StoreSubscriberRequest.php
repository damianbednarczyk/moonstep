<?php

namespace App\Http\Requests;

use App\Models\Subscriber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSubscriberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email:rfc,dns',
                Rule::unique(Subscriber::class, Subscriber::ATTRIBUTE_EMAIL),
            ],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'sometimes', 'string'],
            'tag_ids' => ['nullable', 'sometimes', 'array'],

        ];
    }
}
