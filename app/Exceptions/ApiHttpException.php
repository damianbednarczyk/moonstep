<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class ApiHttpException extends HttpException
{

    const CODE_DEFAULT = 0;

    /**
     * @param string|null $message
     * @param Throwable|null $previous
     * @param int|null $code
     * @param int $statusCode
     * @param array $headers
     */
    public function __construct(
        ?string $message = '',
        ?int $code = self::CODE_DEFAULT,
        Throwable $previous = null,
        int $statusCode = Response::HTTP_BAD_REQUEST,
        array $headers = []
    ) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    /**
     * @return Response
     */
    public function render(): Response
    {
        if (config('app.debug')) {
            $data = [
                'message' => $this->getMessage(),
                'code' => $this->getCode(),
                'exception' => get_class($this),
                'file' => $this->getFile(),
                'line' => $this->getLine(),
                'trace' => collect($this->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all(),
            ];

            if (($previous = $this->getPrevious())) {
                $data['previous_exception'] = get_class($previous);
                $data['previous_message'] = $previous->getMessage();
                $data['previous_line'] = $previous->getLine();
                $data['previous_file'] = $previous->getFile();
                $data['previous_trace'] = $previous->getTrace();
            }
        } else {
            $data = [
                'code' => $this->getCode(),
                'message' => $this->getMessage(),
            ];
        }

        return new JsonResponse($data, $this->getStatusCode());
    }

}
