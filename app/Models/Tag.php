<?php

namespace App\Models;

use App\Models\Interfaces\TagInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Tag extends Model implements TagInterface
{
    use HasFactory , HasSlug;


    protected $guarded = [self::ATTRIBUTE_ID];

    protected $table = self::TABLE_NAME;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(self::ATTRIBUTE_NAME)
            ->saveSlugsTo(self::ATTRIBUTE_SLUG);
    }
}
