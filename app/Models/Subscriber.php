<?php

namespace App\Models;

use App\Models\Interfaces\SubscriberInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Subscriber extends Model implements SubscriberInterface
{
    use HasFactory;

    protected $table = self::TABLE_NAME;

    protected $guarded = [self::ATTRIBUTE_ID];


    /**
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class,'subscriber_tag');
    }


}
