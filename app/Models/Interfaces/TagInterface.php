<?php


namespace App\Models\Interfaces;


interface TagInterface
{

    const TABLE_NAME = 'tags';

    const ATTRIBUTE_SLUG = 'slug';

    const ATTRIBUTE_ID = 'id';

    const ATTRIBUTE_NAME = 'name';

    const ATTRIBUTE_CREATED_AT   = 'created_at';

    const ATTRIBUTE_UPDATED_AT = 'updated_at';


}
