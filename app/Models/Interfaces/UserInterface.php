<?php


namespace App\Models\Interfaces;


interface UserInterface
{

    const TABLE_NAME = 'users';

    const ATTRIBUTE_ID = 'id';

    const ATTRIBUTE_NAME = 'name';

    const ATTRIBUTE_EMAIL = 'email';

    const ATTRIBUTE_PASSWORD = 'password';

    const ATTRIBUTE_REMEMBER_TOKEN = 'remember_token';

    const ATTRIBUTE_EMAIL_VERIFIED_AT  = 'email_verified_at';

    const ATTRIBUTE_CREATED_AT   = 'created_at';

    const ATTRIBUTE_UPDATED_AT = 'updated_at';
}
