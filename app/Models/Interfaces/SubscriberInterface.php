<?php


namespace App\Models\Interfaces;


interface SubscriberInterface
{

    const TABLE_NAME = 'subscribers';

    const ATTRIBUTE_ID = 'id';

    const ATTRIBUTE_FIRST_NAME = 'first_name';

    const ATTRIBUTE_LAST_NAME = 'last_name';

    const ATTRIBUTE_EMAIL = 'email';

    const ATTRIBUTE_CREATED_AT   = 'created_at';

    const ATTRIBUTE_UPDATED_AT = 'updated_at';

}
